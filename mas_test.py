import unittest
from mas import *

class TestMas(unittest.TestCase):
    def test_mas_send(self):
        mas = Mas(user='account.fyscjg', configPath="./config.ini")
        response = mas.send(mobile='13819130258', content='test test test')
        print("response", response)
        self.assertTrue(response['rspcod'] == 'success')
        self.assertTrue(response['success'] == True)


if __name__ == '__main__':
    unittest.main()
