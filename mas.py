import requests
import hashlib, base64, json
import configparser

class Mas(object):
    def __init__(self, user:str, configPath:str):
        self.configPath = configPath
        self.config = configparser.ConfigParser()
        self.config.read(self.configPath)
        self.user = user

    def getMac(self, ecName:str, apId:str, secretKey:str, mobiles:str, content:str, sign:str, addSerial:str):
        md5Str = ecName + apId + secretKey + mobiles + content + sign + addSerial
        return hashlib.md5(md5Str.encode()).hexdigest()

    def getData(self, mobiles:str, content:str):
        params = {}
        accout = self.config[self.user]
        params['ecName'] = accout['ecName']
        params['apId'] = accout['apId']
        params['mobiles'] = mobiles
        params['content'] = content
        params['sign'] = accout['sign']
        if accout.getboolean('accurate'):
            params['addSerial'] = ""
        params['mac'] = self.getMac(ecName=accout['ecName'],
                            apId=accout['apId'],
                            secretKey=accout['secretKey'],
                            mobiles=mobiles,
                            content=content,
                            sign=accout['sign'],
                            addSerial=params['addSerial'])
        data = base64.b64encode(json.dumps(params).encode('utf-8'))
        data = str(data, 'utf-8')
        return data
    
    def send(self, mobile:str, content:str):
        data = self.getData(mobiles=mobile, content=content)
        r = requests.post(url=self.config['URL']['url'], data=data)
        return r.json()


'''
example:
    mas = Mas(user='account.fyscjg', configPath="./config.ini")
    mas.send(mobiles='15267050513', content='测试')
'''

