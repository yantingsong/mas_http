import unittest, os
from handle import *

class TestHandle(unittest.TestCase):
    def setUp(self):
        # prepare, create xlsx file
        self.wb = Workbook()
        work_sheet = self.wb.active
        work_sheet_sup = self.wb.create_sheet('上级抽检')
        work_sheet_sup.append(['序号', '类别', '编号', '企业名称',
                             '属地单位', '经办人姓名', '接收短信号码',
                             '上级交办（移送）材料收件日期', '整改通知书送达日期', '复查申请（停产停售报告）提交日期', '抽样复查日期',
                             '罚没款（万元）', '处理类型', '状态'])
        # 增加一条已提交复查申请，未复查的临近超期记录
        work_sheet_sup.append(['1','系统抽检','2023－024','ABCD商贸有限公司',
                              'ABscjg所', '小宋', '152******13',
                              '2023.12.20','2023.12.21','2024.3.10','',
                              '0.00649','流通抽查－销售者后处理','正在处理'])
        # 增加一条未提交复查申请，未复查的临近超期记录
        work_sheet_sup.append(['2','系统抽检','2023－025','ABCD商贸有限公司',
                               'ABscjg所', '小宋', '152******13',
                               '2023.12.20', '2023.12.21', '', '',
                               '0.00649', '流通抽查－销售者后处理', '正在处理'])
        # 增加一条未提交复查申请，复查申请临近超期记录
        work_sheet_sup.append(['3', '系统抽检', '2023－026', 'ABCD商贸有限公司',
                               'ABscjg所', '小宋', '152******13',
                               '2023.12.20', '2023.12.21', '', '',
                               '0.00649', '流通抽查－销售者后处理', '正在处理'])
        
        work_sheet_loc = self.wb.create_sheet('本级抽检')
        work_sheet_loc.append(['序号', '类别', '编号', '企业名称',
                             '属地单位', '经办人姓名', '接收短信号码',
                             '上级交办（移送）材料收件日期', '抽查结果通知书、检验报告送达日期', '整改通知书送达日期', '复查申请（停产停售报告）提交日期', '抽样复查日期',
                             '罚没款（万元）', '处理类型', '状态'])
        self.wb.save('./test.xlsx')

    def test_check_reexam_sumbit(self):
        # create handler
        handler = workbookHandle('./test.xlsx', 'account.test', './config_test.ini')
        submitClose, reexamClose = handler.getSupCloseToDue()
        self.assertTrue(len(submitClose) == 1)
        self.assertTrue(len(reexamClose) == 2)
        """
        submitClose, reexamClose = handler.getSupCloseToDue()
        self.assertTrue(len(submitClose) == 1)
        self.assertTrue(len(reexamClose) == 1)
        """

    def test_reexam_message(self):
        ''' just print message. no assert'''
        # create handler
        sup = self.wb['上级抽检']
        for row_idx in range(2, sup.max_row):
            s = workbookHandle.reexamMessage(supColName, sup[row_idx])
            print(s)
            s = workbookHandle.reexamMessage(supColName, sup[row_idx])
            print(s)

        local = self.wb['本级抽检']
        for row_idx in range(2, local.max_row):
            s = workbookHandle.reexamMessage(locColName, local[row_idx])
            print(s)
            s = workbookHandle.reexamMessage(locColName, local[row_idx])
            print(s)

    def tearDown(self):
        # clear, remove xlsx file
        os.remove('./test.xlsx')
    


if __name__ == '__main__':
    unittest.main()
