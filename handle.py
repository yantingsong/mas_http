# strip每一个的前后空格
from openpyxl import *
from enum import Enum
from mas import *
import datetime
from dateutil.relativedelta import relativedelta

class supColName(Enum):
    number = 0
    category = 1
    Id = 2
    company_name = 3
    territory = 4
    attn = 5                    # 经办人
    mobile = 6
    matrial_received_date = 7
    rectification_noticed_date = 8
    reexam_submitted_date = 9
    reexam_date = 10
    penalty_anmount = 11
    process_type = 12

class locColName(Enum):
    number = 0
    category = 1
    Id = 2
    company_name = 3
    territory = 4
    attn = 5
    mobile = 6
    matrial_received_date = 7
    exam_noticed_date = 8
    rectification_noticed_date = 9
    reexam_submitted_date = 10
    reexam_date = 11
    penalty_anmount = 12
    process_type = 13

class workbookHandle(object):
    def __init__(self, xlsxPath:str, user:str, configPath:str):
        '''
            read a xlsx file
        '''
        self.wb = load_workbook(filename=xlsxPath)
        self.mas = Mas(user=user, configPath=configPath)
        active_sheets = self.wb.active
        print("active sheets:", active_sheets)

    @classmethod
    def checkReexamSumbit(self, Type:Enum, row):
        '''
            复查申请是否超期： 收到整改通知书之日起60日内
            return:
                False: 已经提交复查申请/还没有收到整改通知书/期限没有超过55(60-5)日
                True: 期限已经满55(60-5)日,临近超期
        '''
        today = datetime.date.today()
        reexam_submitted_date_cell = row[Type.reexam_submitted_date.value]
        # 已经提交了复查申请
        if reexam_submitted_date_cell.value:
            return False

        noticed_date_cell = row[Type.rectification_noticed_date.value]

        # 还没有收到整改通知书
        if not noticed_date_cell.value:
            return False  
        # 期限没有超过55(60-5)日 
        noticed_date = datetime.datetime.strptime(noticed_date_cell.value,'%Y.%m.%d').date()
        delta = (today - noticed_date).days
        return delta >= 55

    @classmethod
    def checkReexam(self, Type:Enum, row):
        '''
            复查是否超期： 收到复查申请之日起15日内; 或收到责令整改通知书起75日内
            return:
                False: 已经复查/整改通知书和复查申请都没有收到/收到复查申请之日起未满12日 且 收到责令整改通知书起未满70日
                True:  收到复查申请之日起满12日(15-3); 或收到责令整改通知书起满70日(70-5),临近超期
        '''
        today = datetime.date.today()
        reexam_date_cell = row[Type.reexam_date.value]
        # 已经抽样复查
        if reexam_date_cell.value:
            return False

        reexam_submitted_date_cell = row[Type.reexam_submitted_date.value]
        noticed_date_cell = row[Type.rectification_noticed_date.value]

        # 整改通知书和复查申请都没有收到
        if not noticed_date_cell.value and not reexam_submitted_date_cell:
            return False
        # 收到责令整改通知书起是否满70日
        if noticed_date_cell.value:
            noticed_date = datetime.datetime.strptime(noticed_date_cell.value,'%Y.%m.%d').date()
            notice_delta = (today - noticed_date).days
            if notice_delta >= 70: return True
        # 收到复查申请之日是否起满12日
        if reexam_submitted_date_cell.value:
            reexam_submitted_date = datetime.datetime.strptime(noticed_date_cell.value,'%Y.%m.%d').date()
            reexam_delta = (today - noticed_date).days
            if reexam_delta >= 12: return True
        return False



    def handleSuperior(self):
        '''处理上级抽检子表'''
        sheet_name = '上级抽检'
        if sheet_name not in self.wb.sheetnames:
            print("找不到" + sheet_name)
            return

        superior = self.wb[sheet_name]
        for row_idx in range(2, superior.max_row):
            self.checkReexamSumbit(supColName, superior[row_idx])
            self.checkReexam(supColName, superior[row_idx])

    def handleLocal(self):
        '''处理本级抽检子表'''
        sheet_name = '本级抽检'
        if sheet_name not in self.wb.sheetnames:
            print("找不到" + sheet_name)
            return
        
        local = self.wb[sheet_name]
        for row_idx in range(2, local.max_row):
            if self.checkReexamSumbit(locColName, local[row_idx]):
                self.sendReexamSubmitMessage(locColName, local[row_idx])
            if self.checkReexam(locColName, local[row_idx]):
                self.sendReexamSubmitMessage(locColName, local[row_idx])

    def getSupCloseToDue(self):
        '''获取上级抽检子表临近超期的项'''
        sheet_name = '上级抽检'
        if sheet_name not in self.wb.sheetnames:
            print("找不到" + sheet_name)
            return
        submitClose, reexamClose = [], []
        superior = self.wb[sheet_name]
        for row_idx in range(2, superior.max_row):
            if self.checkReexamSumbit(supColName, superior[row_idx]):
                submitClose.append(superior[row_idx])
            if self.checkReexam(supColName, superior[row_idx]):
                reexamClose.append(superior[row_idx])
        return submitClose, reexamClose

    def getLocalCloseToDue(self):
        '''获取本级抽检子表临近超期的项'''
        sheet_name = '本级抽检'
        if sheet_name not in self.wb.sheetnames:
            print("找不到" + sheet_name)
            return

        submitClose, reexamClose = [], []
        local = self.wb[sheet_name]
        for row_idx in range(2, local.max_row):
            if self.checkReexamSumbit(locColName, local[row_idx]):
                submitClose.append(local[row_idx])
            if self.checkReexam(locColName, local[row_idx]):
                reexamClose.append(local[row_idx])
        return submitClose, reexamClose

    @staticmethod
    def reexamSubmitMessage(Type:Enum, row) -> str:
        ''' build message content '''
        attn = row[Type.attn.value].value
        Id = row[Type.Id.value].value
        company_name = row[Type.company_name.value].value
        material_date = row[Type.matrial_received_date.value].value
        noticed_date = row[Type.rectification_noticed_date.value].value
        noticed_day = datetime.datetime.strptime(noticed_date,'%Y.%m.%d').date()
        submit_due_date = noticed_day + relativedelta(days=15)
        delta = (submit_due_date - datetime.date.today()).days

        s = '%s您好，您负责经办的案件抽样复查申请即将超期，抽检编码为%s，抽检企业名称为%s，抽检材料接受日期为%s，收到责令整改通知书日为%s，抽样复查申请的截止日期为%s(收到责令整改通知书后15日内)，距离超期还有%s天，请及时处置。' % \
            (attn, Id, company_name, material_date, noticed_date, submit_due_date, delta)
        return s

    @staticmethod
    def reexamMessage(Type: Enum, row) -> str :
        ''' build message content '''
        attn = row[Type.attn.value].value
        Id = row[Type.Id.value].value
        company_name = row[Type.company_name.value].value
        material_date = row[Type.matrial_received_date.value].value
        noticed_date = row[Type.rectification_noticed_date.value].value
        submit_date = row[Type.reexam_submitted_date.value].value

        if not submit_date:
            noticed_day = datetime.datetime.strptime(noticed_date,'%Y.%m.%d').date()
            reexam_due_date = noticed_day + relativedelta(days=75)
            delta = (reexam_due_date - datetime.date.today()).days
            s = '%s您好，您负责经办的案件复查抽样即将超期，抽检编码为%s，抽检企业名称为%s，抽检材料接受日期为%s，收到责令整改通知书日为%s，未收到复查申请，抽样复查日期的截止日期为%s(收到责令整改通知书后75日)，距离超期还有%s天，请及时处置。' % \
                (attn, Id, company_name, material_date, noticed_date, reexam_due_date, delta)
            return s

        submit_day = datetime.datetime.strptime(submit_date,'%Y.%m.%d').date()
        reexam_due_date = submit_day + relativedelta(days=15)
        delta = (reexam_due_date - datetime.date.today()).days
        s = '%s您好，您负责经办的案件复查抽样即将超期，抽检编码为%s，抽检企业名称为%s，抽检材料接受日期为%s，收到责令整改通知书日为%s，复查申请日期为%s，抽样复查日期的截止日期为%s(收到复查申请后15日内)，距离超期还有%s天，请及时处置。' % \
            (attn, Id, company_name, material_date, noticed_date, submit_date, reexam_due_date, delta)
        return s


    def sendReexamSubmitMessage(self, Type:Enum, row):
        ''' 发送复查申请提醒 '''
        content = workbookHandle.reexamSubmitMessage(row)
        self.mas.send(mobile=row[Type.mobile.value], content=content)

    def sendReexamMessage(self, Type:Enum, row):
        '''发送复查提醒'''
        content = workbookHandle.reexamMessage(row)
        self.mas.send(mobile=row[Type.mobile.value], content=content)


'''
example:
    wbh = workbookHandle(xlsxPath = './抽检后处理表.xlsx',
                        user='account.fyscjg',
                        configPath="./config.ini")
    wbh.handleSuperior()
    wbh.handleLocal()
'''