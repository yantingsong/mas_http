from flask import Flask, jsonify, request
from handle import *

app = Flask(__name__)

@app.route('/login')
def helloWorld():
    return 'Hello World!'

@app.route('/getclosedue', methods = ['GET'])
def getCloseDue():
    ''' 
    # Always read the newest file 
    wbh = workbookHandle(xlsxPath = './抽检后处理表.xlsx',
                    user='account.fyscjg',
                    configPath="./config.ini")
    supSubmitClose, supReexamClose = wbh.getSupCloseToDue()
    locSubmitClose, locReexamClose = wbh.getLocalCloseToDue()
    '''
    return None

if __name__ == '__main__':
    app.run(host="0.0.0.0",
            port=8091,
            ssl_context=(
                'pem/server-cert.pem',
                'pem/server-key.pem'
                )
            )